# Domain Path

Adds the ability to provide different path aliases for an entity across domains.
Also allows the same alias to be used for different entities as long as it is on
different domains. This enables us to, for example, have a separate node per
domain, each with an alias "/about-us".

# Installation

Download and install as usual:

https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules

# Usage

- Once installed you can toggle domain paths on a per-entity-type basis on the
main configuration page (/admin/config/domain_path/domain_path_settings).
    - Nodes are enabled by default.
- When editing an entity with domain_paths enabled, there will be a fieldset
with a textfield per-domain to enter aliases for.
    - On entities with domain access settings (e.g. nodes), only the domains
    that are currently being published to will have domain path fields visible.
- If no domain path exists for an entity, the default path module URL alias will
be used.
- If there is a domain path it will override the default URL alias.
